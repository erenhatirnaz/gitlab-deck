import axios from 'axios';
import { getToken } from './utils';

const glResource = axios.create({
  baseURL: 'https://gitlab.com/api/v4/',
});

glResource.interceptors.request.use(
  config => {
    config.headers['PRIVATE-TOKEN'] = getToken();
    return config;
  },
  error => Promise.reject(error)
);

export default {
  updateSelectedProjectId({ commit }, id){
    commit('setSelectedProjectId', id);
  },
  toggleLoading({ commit }) {
    commit('toggleLoading');
  },
  toggleToken({ commit }) {
    commit('toggleToken');
  },
  async fetchProjects({ commit }) {
    const PROJECTS_PATH = 'projects?starred=true&simple=true&order_by=last_activity_at';
    const projectRes = await glResource.get(PROJECTS_PATH);

    commit('setProjects', projectRes.data);
  },
  async fetchUser({ commit }) {
    const USER_PATH = 'user';
    const userRes = await glResource.get(USER_PATH);

    commit('setUser', userRes.data);
  },
  async fetchIssues({ commit, state }) {
    const ISSUES_PATH = `projects/${state.selectedProjectId}/issues?scope=assigned-to-me&state=opened`;
    const { data } = await glResource.get(ISSUES_PATH);

    commit('setIssues', data);
  },
  async fetchAssignedMRs({ commit, state }) {
    const ASSIGNED_MRS_PATH = `projects/${state.selectedProjectId}/merge_requests?scope=assigned-to-me&state=opened`;
    const { data } = await glResource.get(ASSIGNED_MRS_PATH);

    commit('setAssignedMRs', data);
  },
  async fetchCreatedMRs({ commit, state }) {
    const CREATED_MRS_PATH = `projects/${state.selectedProjectId}/merge_requests?scope=created-by-me&state=opened`;
    const { data } = await glResource.get(CREATED_MRS_PATH);

    commit('setCreatedMRs', data);
  },
  async fetchPipelines({ commit, state }) {
    const { username } = state.user;
    const PIPELINES_PATH = `projects/${state.selectedProjectId}/pipelines?username=${username}`;
    const { data } = await glResource.get(PIPELINES_PATH);

    commit('setPipelines', data);
  },
};
